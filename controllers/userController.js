// Require model so we could use the model for searching
const User = require("../models/User.js");
const Course = require('../models/Course');

const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.checkEmailExist = (reqBody) => {

	// ".find" - a mongoose crud operation (query) to find a field value from a collection
	return User.find({email: reqBody.email }).then(result => {
		// condition if there is an exsiting  user
		if(result.length > 0){
			return true;
		}
		// condition if there is no existing user
		else
		{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		/*
			// bcrypt - package for password hashing
			// hashSync - synchronously generate a hash
			// hash - asynchoursly generate a hash
		*/
		// hashing - converts a value to another value
		password: bcrypt.hashSync(reqBody.password, 10),
		/*
			// 10 = salt rounds
			// Salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds, the longer it takes to generate an output 
		*/
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			// compareSync is bcrypt function to compare a unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password do not match
				return false;
				// return "Incorrect password"
			}
		}
	})
}

// S38 Activity Possible Answers 

// A
module.exports.getProfileA = (reqBody) => {
	return User.findById(reqBody.userId).then((details, err) => {
		if(err){
			return false;
		}
		else{
			details.password = "*****";
			return details;
		}
	})
}

// B
module.exports.getProfileB = (userId) => {
	return User.findById(userId).then((details, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			details.password = "";
			return details;
		}
	})
};


// Enroll user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {

	// Add the course ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
	let isUserUpdated = await User.findById(data.userId).then(user => {

		// Adds the courseId in the user's enrollments array
		user.enrollments.push({courseId : data.courseId});

		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		// Saves the updated course information in the database
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

	// Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	// User enrollment failure
	} else {
		return false;
	};

};

